Installation
============
- clone repo `git clone git@gitlab.com:anton.sivolapov/plane-game.git`
- go to newly created folder `cd plane-game`
- start any http server on `www` folder. Example with **http-server** `npm i -g http-server && http-server -p 8080 ./www`
- open browser on `localhost:8080`
- play
